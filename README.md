# xpackage-api

[TOC]

## 1. 文档说明

### API访问域名

正式域名：http://xxxx

测试域名：http://xxxx

### 公共参数

调用任何一个API都必须传入的参数，目前支持的公共参数有：

|参数            |类型    |必选      |        说明|
|:--------------|:------------|:-------- |:-----------|
|v               |String  |是        |API协议版本号。当前值为1.0 |
|app_key			 |String  |是		   |应用ID。值为：21148613 |
|timestamp       |String  |是        |	时间戳，格式为yyyy-MM-dd  HH:mm:ss，时区为GMT+8，例如：2015-01-01 12:00:00。API服务端允许客户端请求最大时间误差为10分钟。|
|sign             |String  |是        |API输入参数签名结果 |
|session	        |String  |否        |用户登录授权成功后，颁发给应用的授权信息。当此API的标签上注明：“需要授权”，则此参数必传；“不需要授权”，则此参数不需要传；“可选授权”，则此参数为可选。

### 业务参数

API调用除了必须包含公共参数外，如果API本身有业务级的参数也必须传入，每个API的业务级参数请考API文档说明。



### 签名算法


为了防止API调用过程中被黑客恶意篡改，调用任何一个API都需要携带签名，服务端会根据请求参数，对签名进行验证，签名不合法的请求将会被拒绝。目前支持的签名算法有：MD5。签名大体过程如下：

* 对所有API请求参数（包括公共参数和业务参数，但除去sign参数和byte[]类型的参数），根据参数名称的ASCII码表的顺序排序。如：foo:1, bar:2, foo_bar:3, foobar:4排序后的顺序是bar:2, foo:1, foo_bar:3, foobar:4。
* 将排序好的参数名和参数值拼装在一起，根据上面的示例得到的结果为：bar2foo1foo_bar3foobar4。
* 把拼装好的字符串采用utf-8编码，使用签名算法对编码后的字节流进行摘要。如果使用MD5算法，则需要在拼装的字符串前后加上app的secret后，再进行摘要，如：md5(secret+bar2foo1foo_bar3foobar4+secret)；
* 将摘要得到的字节流结果使用十六进制表示，如：hex(“helloworld”.getBytes(“utf-8”)) = “68656C6C6F776F726C64”

说明：MD5是128位长度的摘要算法，用16进制表示，一个十六进制的字符能表示4个位，所以签名后的字符串长度固定为32个十六进制字符。

**重要： secret值：38ea36fd0789eb4abaf8adf5c8fcdfc8**

### 返回结果

```
{
    status:0, 
    message:,
    body: {
        
    }
}
```
返回字段

| 返回字段   | 类型   |  说明  |
| --------   | -----:  | :----:  |
| status     | Number |   返回结果状态。0：正常；1：错误;                                    其他值：参考具体接口             |
| message    |  String   |   错误描述信息               |
| body       |  Object     |  接口业务数据                    |


## 2. 接口定义

### 2.1 短信

#### 2.1.1 验证码

##### 接口功能

> 获取短信验证码

#### URL

> /sms/verify

##### 支持格式

> JSON

##### HTTP请求方式

> POST

##### 请求参数

|参数   |必选   |类型   |说明   |
|:----- |:-------|:-----|----- |
|phone |ture |string|手机号 |

##### 接口示例

```
curl -X POST 'http://localhost:8080/api/kkuser/verify' \
-H 'Content-Type:application/x-www-form-urlencoded;charset=utf-8' \
-d 'phone=133333333' 
```

##### 返回结果

```
{
    "status": 0,
    "message": "成功",
    "body": {
        "verify_code": "719949"
    }
}
```

##### 字段说明

> 结果字段说明

|返回字段|必选 |字段类型|说明 |
|:-----|:------ |:------|:----------------------------- |
|status |true  |int |返回结果状态。0：正常；1：错误。 |
|message | false |string | 错误信息 |
|body | true |JSON |所属类型 |

> body字段说明

|返回字段|必选 |字段类型|说明 |
|:-----|:------ |:------|:----------------------------- |
|verify_code |true  |int |验证码 |


### 2.1 用户

#### 2.1.1 注册

##### 接口功能

> 注册用户

##### URL

> /user/register

##### 支持格式

> JSON

##### HTTP请求方式

> POST

##### 请求参数

|参数   |必选   |类型   |说明   |
|:----- |:-------|:-----|----- |
|phone |ture |string|手机号 |
|password |ture |string|密码 |
|verify_code |true |int |验证码|


##### 接口示例

```

curl -X POST 'http://139.224.11.153:8090/myapp/registerusersinfo_wh' \
-H 'Content-Type:application/x-www-form-urlencoded;charset=utf-8' \
-d 'did=1000012345' \
-d 'loginname=test_01' \
-d 'password=123456' \
-d 'nickname=test_nickname' \
-d 'verifycode=1234' \
-d 'email=test@163.com' \
-d 'guardianphone=13488886666' \
-d 'guardian=1' \
-d 'sex=1' \
-d 'appid=54f950ab157c9e7a' \
-d 'v=2.0' \
-d 'appKey=2d714b90-4c5c-11e5-98e7-9bf393a32200' \
-d 'sign=60228a6eb407456a53a6c6b05b0dbf0ab5d9e35c' \
-d '_uuid=54f950ab157c9e7a' \
-d 'access_token=mcB7Q4e1bMwnbtCmy2CTp4cAyUzZKZfNC6PcLeX%2Fz6A1O6ndSinIt%2FHq0d5Pp21azpwMx%2FKfYKk1UGwKJf777g%3D%3D_22899'

```

##### 返回结果

```
{
	"state": 0,
	"company": "可口可乐",
	"category": "饮料"
}
```

##### 字段说明

|返回字段|字段类型|说明 |
|:----- |:------|:----------------------------- |
|status | int |返回结果状态。0：正常；1：错误。 |
|company | string | 所属公司名 |
|category | string |所属类型 |







### 2.2 项目







	


